package com.seb.blueradio;

import android.annotation.SuppressLint;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.Objects;
import java.util.UUID;


public class RadioControl extends AppCompatActivity implements View.OnClickListener {
    private boolean isBtConnected = false;
    private BluetoothSocket mmSocket = null;
    private BluetoothDevice blueDevice;
    private final static UUID MY_UUID = UUID.fromString("00001101-0000-1000-8000-00805f9b34fb");


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_radio_control);
        blueDevice = Objects.requireNonNull(getIntent().getExtras()).getParcelable("btDeviceInUse");
        new ConnectBT(this).execute();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onStart() {
        super.onStart();

        TextView rds_info;
        Button vol_m, vol_p, freq_m, freq_p, scan_m, scan_p, rds_button;

        String CONNECTED_DEVICE = blueDevice.getName() + "\n" + blueDevice.getAddress();

        vol_p = findViewById(R.id.vol_p);
        vol_m = findViewById(R.id.vol_m);
        freq_p = findViewById(R.id.freq_p);
        freq_m = findViewById(R.id.freq_m);
        scan_p = findViewById(R.id.scan_p);
        scan_m = findViewById(R.id.scan_m);
        rds_button = findViewById(R.id.rds_func);
        rds_info = findViewById(R.id.RDS);
        //  freq_info = findViewById(R.id.freq_info);

        rds_info.setText(CONNECTED_DEVICE);
        vol_p.setOnClickListener(this);
        vol_m.setOnClickListener(this);
        freq_p.setOnClickListener(this);
        freq_m.setOnClickListener(this);
        scan_p.setOnClickListener(this);
        scan_m.setOnClickListener(this);
        rds_button.setOnClickListener(this);


    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.vol_m:
                sendMessage("vol_m");
                break;

            case R.id.vol_p:
                sendMessage("vol_p");
                break;

            case R.id.freq_m:
                sendMessage("freq_m");
                break;

            case R.id.freq_p:
                sendMessage("freq_p");
                break;

            case R.id.scan_m:
                sendMessage("scan_m");
                break;

            case R.id.scan_p:
                sendMessage("scan_p");
                break;

            case R.id.rds_func:
                sendMessage("rds");
                break;

            default:

                break;
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        try {
            mmSocket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void sendMessage(String word) {
        if (mmSocket != null) {
            try {
                mmSocket.getOutputStream().write(word.getBytes());/// TA WSPANIALA KOMENDA DZIALA!!!!!!!!!!!!!!!!!!!!!!!!!!!
            } catch (IOException e) {
                Log.e("Error", "Error");
            }
        }
    }


    @SuppressLint("StaticFieldLeak")
    private class ConnectBT extends AsyncTask<Void, Void, Void>  // UI thread
    {
        private boolean ConnectSuccess = true; //if it's here, it's almost connected
        private WeakReference<RadioControl> activityReference;

        ConnectBT(RadioControl context) {
            activityReference = new WeakReference<>(context);
        }

        @Override
        protected Void doInBackground(Void... devices) //while the progress dialog is shown, the connection is done in background
        {
            try {
                if (mmSocket == null || !isBtConnected) {
                    BluetoothAdapter ba = BluetoothAdapter.getDefaultAdapter();
                    BluetoothDevice temporaryDevice = ba.getRemoteDevice(blueDevice.getAddress());//connects to the device's address and checks if it's available
                    mmSocket = temporaryDevice.createInsecureRfcommSocketToServiceRecord(MY_UUID);//create a RFCOMM (SPP) connection
                    BluetoothAdapter.getDefaultAdapter().cancelDiscovery();
                    mmSocket.connect();//start connection
                }
            } catch (IOException e) {
                ConnectSuccess = false;//if the try failed, you can check the exception here
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) //after the doInBackground, it checks if everything went fine
        {
            super.onPostExecute(result);
            RadioControl activity = activityReference.get();
            if (activity == null || activity.isFinishing()) return;

            if (!ConnectSuccess) {
                Log.d("NOWE BTCONNECTION INFOOO", "Connection Failed. Is it a SPP Bluetooth? Try again.");
                finish();
            } else {
                Log.d("NOWE BTCONNECTION INFOOO", "Connected.");

                isBtConnected = true;
            }
        }
    }
}


