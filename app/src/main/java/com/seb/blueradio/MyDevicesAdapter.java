package com.seb.blueradio;

import android.app.Activity;
import android.app.AlertDialog;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Handler;

public class MyDevicesAdapter extends RecyclerView.Adapter {

    private List<BluetoothDevice> devicesList = new ArrayList<>();
    private Context context;
    private RecyclerView recyclerView;
    private BluetoothDevice selectedDevice;
    private BluetoothDevicesFinder bluetoothDevicesFinder;

    public MyDevicesAdapter(Context context, List<BluetoothDevice> devicesList, RecyclerView recyclerView, BluetoothDevicesFinder bluetoothDevicesFinder) {
        this.devicesList = devicesList;
        this.recyclerView = recyclerView;
        this.context = context;
        this.bluetoothDevicesFinder = bluetoothDevicesFinder;
    }


    private class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView deviceName;
        public TextView deviceMacAddress;


        public MyViewHolder(View pItem) {
            super(pItem);
            deviceName = pItem.findViewById(R.id.deviceName);
            deviceMacAddress = pItem.findViewById(R.id.deviceMac);
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, final int i) {
        // create a new view

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.devices_list, viewGroup, false);

        view.setOnClickListener(v -> {
            int position = recyclerView.getChildAdapterPosition(v);
            selectedDevice = devicesList.get(position);
            Toast.makeText(context, "Nacisnięto " + selectedDevice.getName(), Toast.LENGTH_SHORT).show();
            switch (selectedDevice.getBondState()) {
                case BluetoothDevice.BOND_NONE:
                    try {
                        bluetoothDevicesFinder.createBond(selectedDevice);
                        notifyDataSetChanged();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                case BluetoothDevice.BOND_BONDED:
                    showDialog(v, "Wybierz opcję", "Wybrano sparowane urządzenie Bluetooth: " + selectedDevice.getName() + ". Połączyć czy usunąć?", selectedDevice, position);
                    break;
            }
            // showDialog(v,"Wybierz opcję", "Wybrano sparowane urządzenie Bluetooth: " + selectedDevice.getName() +    ". Połączyć czy usunąć?", selectedDevice, position);

        });
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        // uzupelnia liste elementami
        BluetoothDevice device = devicesList.get(i);
        ((MyViewHolder) viewHolder).deviceName.setText(device.getName());
        ((MyViewHolder) viewHolder).deviceMacAddress.setText(device.getAddress());

    }

    @Override
    public int getItemCount() {
        return devicesList.size();

    }

    private void showDialog(View view, String title, CharSequence message, final BluetoothDevice chosenDevice, final int position) { // zamiast Context context bylo Activity activity -> Context jest pochodna od Activity

        AlertDialog.Builder dialogBox = new AlertDialog.Builder(view.getContext());
        if (title != null) dialogBox.setTitle(title);

        dialogBox.setMessage(message);
        dialogBox.setPositiveButton("Połącz", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                Intent intent = new Intent(view.getContext(), RadioControl.class);
                intent.putExtra("btDeviceInUse", chosenDevice);

                bluetoothDevicesFinder.connectWithPairedDevice(chosenDevice);
                if (chosenDevice.getBondState() == BluetoothDevice.BOND_BONDED) {
                    view.getContext().startActivity(intent);
                    // startActivity();

                } else {
                    Toast.makeText(recyclerView.getContext(), "Nie można połączyć się z " + chosenDevice.getName(), Toast.LENGTH_SHORT).show();
                }
            }
        });

        dialogBox.setNegativeButton("Usuń", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {

                bluetoothDevicesFinder.removeBondForDevice(position);
                notifyDataSetChanged();
                //     blueHandler.post(pairedDevicesViewThread);
                //------------------------------------->>>>>>>>>>>>updateAdapter(pairedDevicesAdapter, pairedList);
                // updateAdapter(foundDevicesAdapter, foundDevicesList);

            }
        });
        dialogBox.show();
    }


}
