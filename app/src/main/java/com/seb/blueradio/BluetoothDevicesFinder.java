package com.seb.blueradio;

import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.util.Log;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class BluetoothDevicesFinder {
    private Context mContext;

    private BluetoothDevice selectedDevice;

    ArrayList<BluetoothDevice> listOfAllDevices = new ArrayList<>();

    List<BluetoothDevice> streamPairedList = new ArrayList<>();
    List<BluetoothDevice> streamUnpairedList = new ArrayList<>();

    ArrayList<BluetoothDevice> arrayListUnpairedList = new ArrayList<>();
    ArrayList<BluetoothDevice> arrayListPairedList = new ArrayList<>();

    public BluetoothDevicesFinder(Context mContext, ArrayList<BluetoothDevice> listOfAllDevices, ArrayList<BluetoothDevice> pairedDevicesList, ArrayList<BluetoothDevice> unpairedDevicesList) {
        this.mContext = mContext;
        this.listOfAllDevices = listOfAllDevices;
        this.arrayListPairedList = pairedDevicesList;
        this.arrayListUnpairedList = unpairedDevicesList;
    }

    void createBondForFoundDevice(int position) {
        if (!arrayListUnpairedList.isEmpty()) {

            selectedDevice = arrayListUnpairedList.get(position);
            try {
                createBond(selectedDevice);

                arrayListPairedList.add(selectedDevice);
                arrayListUnpairedList.remove(selectedDevice);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    void removeBondForDevice(int position) {
      
        selectedDevice = arrayListPairedList.get(position);
        try {

            removeBond(selectedDevice);
            arrayListPairedList.remove(position);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    void classifyDevices(ArrayList<BluetoothDevice> list) {

        for (BluetoothDevice device : list) {
            if (device.getBondState() == BluetoothDevice.BOND_BONDED) {
                arrayListPairedList.add(device);
            } else {
                arrayListUnpairedList.add(device);
            }
        }

    }

    boolean createBond(BluetoothDevice btDevice) throws Exception {
        Class bondClass = Class.forName("android.bluetooth.BluetoothDevice");
        Method createBondMethod = bondClass.getMethod("createBond");
        return (boolean) createBondMethod.invoke(btDevice);

    }//createBond()

    boolean removeBond(BluetoothDevice btDevice) throws Exception {
        Class btClass = Class.forName("android.bluetooth.BluetoothDevice");
        Method removeBondMethod = btClass.getMethod("removeBond");
        return (boolean) removeBondMethod.invoke(btDevice);

    }// removeBond()


    void assignFoundDevice(BluetoothDevice newDevice) {

        if (!listOfAllDevices.contains(newDevice)) {

            listOfAllDevices.add(newDevice);
            classifyDevices(listOfAllDevices);
        }
    }


    List<BluetoothDevice> getPairedDevicesList() {
        return streamPairedList;
    }

    List<BluetoothDevice> getunpairedDevicesList() {
        return streamUnpairedList;
    }

    void connectWithPairedDevice(BluetoothDevice bDevice) {
        if (bDevice.getBondState() != BluetoothDevice.BOND_BONDED) {
            bDevice.createBond();
        }

    }

}
