package com.seb.blueradio;

import android.Manifest;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.util.ArrayList;

public class MainMenu extends AppCompatActivity implements View.OnClickListener {

    private RecyclerView pairedDevicesRecyclerView, foundDevicesRecyclerView;
    private RecyclerView.Adapter foundRecyclerAdapter, pairedRecyclerAdapter;
    private LinearLayoutManager foundLayoutManager, pairedLayoutManager;


    //-------------------------------------------------------------------------------------
    private Runnable updateDevicesListsThread;
    private Handler blueHandler = new Handler(Looper.getMainLooper());
    //-------------------------------------------------------------------------------------
    private BluetoothAdapter bluetoothAdapter;
    private BluetoothDevice selectedDevice;

    //++++++++++ private ArrayList<BluetoothDevice> pairedDevicesList = new ArrayList<>();
    private ArrayList<BluetoothDevice> listOfAllDevices;
    private ArrayList<BluetoothDevice> pairedDevicesList, unpairedDevicesList;// = new ArrayList<>(); // stare listOfAllDevices


    private IntentFilter filter;// = new IntentFilter(); //BluetoothDevice.ACTION_FOUND);
    Button searchButton;
    private BluetoothDevicesFinder bluetoothDevicesFinder;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);

        int MY_PERMISSIONS_REQUEST_ACCESS_COARSE_LOCATION = 1;
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
                MY_PERMISSIONS_REQUEST_ACCESS_COARSE_LOCATION);

        //  IntentFilter filter = new IntentFilter();

//----------------------------------------------------------------------------------------------------------------------------

        listOfAllDevices = new ArrayList<>();
        pairedDevicesList = new ArrayList<>();
        unpairedDevicesList = new ArrayList<>();
        filter = new IntentFilter();

//----------------------------------------------------------------------------------------------------------------------------

        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        //this.registerReceiver(btReceiver, filter);

        filter.addAction(BluetoothAdapter.ACTION_STATE_CHANGED);
        filter.addAction(BluetoothDevice.ACTION_FOUND);
        filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_STARTED);
        filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);

        this.registerReceiver(btReceiver, filter);

        searchButton = findViewById(R.id.searchButton);

        searchButton.setOnClickListener(this);

//        bluetoothDevicesFinder = new BluetoothDevicesFinder(getApplicationContext(), listOfAllDevices, pairedDevicesList, unpairedDevicesList);
        bluetoothDevicesFinder = new BluetoothDevicesFinder(getApplicationContext(), listOfAllDevices, pairedDevicesList, unpairedDevicesList);


        pairedDevicesRecyclerView = findViewById(R.id.btPairedDevicesRecyclerView);
        foundDevicesRecyclerView = findViewById(R.id.btDevicesRecyclerView);

        foundLayoutManager = new LinearLayoutManager(getApplicationContext());
        foundDevicesRecyclerView.setLayoutManager(foundLayoutManager);

        pairedLayoutManager = new LinearLayoutManager(getApplicationContext());
        pairedDevicesRecyclerView.setLayoutManager(pairedLayoutManager);
        /*TODO
       ZMIENILEM List<BluetoothDevice> na ArrayList i wtedy sie wyswietla ladnie

         */
     //   foundRecyclerAdapter = new MyFoundDevicesAdapter(getApplicationContext(), unpairedDevicesList, foundDevicesRecyclerView, bluetoothDevicesFinder);
        foundRecyclerAdapter = new MyDevicesAdapter(getApplicationContext(), unpairedDevicesList, foundDevicesRecyclerView, bluetoothDevicesFinder);

        pairedRecyclerAdapter = new MyDevicesAdapter(getApplicationContext(), pairedDevicesList, pairedDevicesRecyclerView, bluetoothDevicesFinder);

        foundDevicesRecyclerView.setItemAnimator(new DefaultItemAnimator());
        pairedDevicesRecyclerView.setItemAnimator(new DefaultItemAnimator());

        foundDevicesRecyclerView.setAdapter(foundRecyclerAdapter);
        pairedDevicesRecyclerView.setAdapter(pairedRecyclerAdapter);

        blueHandler.post(updateDevicesListsThread);

        //++++++++++ bluetoothDevicesFinder = new BluetoothDevicesFinder(getApplicationContext(), bluetoothAdapter, foundDevicesList, pairedList, pairedDevicesList, foundDevicesAdapter, pairedDevicesAdapter);
        // bluetoothDevicesFinder = new BluetoothDevicesFinder(getApplicationContext(), bluetoothAdapter, foundDevicesList, pairedList, pairedDevicesList, foundDevicesAdapter, pairedDevicesAdapter);


    }

    @Override
    protected void onResume() {
        super.onResume();
    } // onResume


    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.searchButton:

                if (!bluetoothAdapter.isDiscovering()) {
                    unpairedDevicesList.clear();
                    pairedDevicesList.clear();
                    foundRecyclerAdapter.notifyDataSetChanged();
                    pairedRecyclerAdapter.notifyDataSetChanged();

                    Toast.makeText(this, "Rozpoczynam wyszukiwanie", Toast.LENGTH_SHORT).show();

                    bluetoothAdapter.startDiscovery();

                    //  blueHandler.post(updateDevicesListsThread);

                    // checkBtAdapterEnabled(bluetoothAdapter);
                    // searchBtDevices();
                    Toast.makeText(this, "found " + unpairedDevicesList.size(), Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(this, "Szukam...", Toast.LENGTH_SHORT).show();
                    //   Toast.makeText(this, "found " + unpairedDevicesList.size(), Toast.LENGTH_SHORT).show();

                }
                //    blueHandler.post(pairedDevicesViewThread);

                break;
        }
    }// onClick()

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent i) {
        if (resultCode == Activity.RESULT_OK) {
            Log.d("INFO_OK", "Wlaczony BT");
            bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        }
    }// onActivityResult()

    private void checkBtAdapterEnabled(BluetoothAdapter ba) {

        if (!ba.isEnabled()) {
            Intent i = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(i, 1);
        }
    }// checkBtAdapterEnabled()


    private final BroadcastReceiver btReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

       /*     if (action.equals(BluetoothAdapter.ACTION_DISCOVERY_STARTED)) {

                // if (action.equals(BluetoothDevice.ACTION_FOUND)) {
                BluetoothDevice newDevice = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                bluetoothDevicesFinder.assignFoundDevice(newDevice);

            } else if (action.equals(BluetoothAdapter.ACTION_DISCOVERY_FINISHED)) {
                Toast.makeText(context, "Zakonczono wyszukiwanie.", Toast.LENGTH_SHORT).show();

            }
            */

            if (action.equals(BluetoothDevice.ACTION_FOUND)) {
                BluetoothDevice foundDevice = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                // bluetoothDevicesFinder.assignFoundDevice(newDevice);

                bluetoothDevicesFinder.assignFoundDevice(foundDevice);
                blueHandler.post(updateDevicesListsThread);
                //   listOfAllDevices.clear();
            }



          /* if (action.equals(BluetoothAdapter.ACTION_DISCOVERY_FINISHED)) {
                Toast.makeText(context, "Zakonczono wyszukiwanie.", Toast.LENGTH_SHORT).show();
                //       blueHandler.post(pairedDevicesViewThread);


*/
        }// onReceive()

    };


    @Override
    protected void onStop() {
        super.onStop();
        blueHandler.removeCallbacks(null);
    }

    @Override
    protected void onStart() {
        super.onStart();
        checkBtAdapterEnabled(bluetoothAdapter);
        refreshDevicesListThread();
        // this.registerReceiver(btReceiver, filter);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (bluetoothAdapter != null) {
            bluetoothAdapter.cancelDiscovery();
        }
        //  this.unregisterReceiver(btReceiver); // crashowalo program
    }//onDestroy()

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (bluetoothAdapter != null) {
            bluetoothAdapter.cancelDiscovery();

        }
        try {
            this.unregisterReceiver(btReceiver);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }//onDestroy()


    void refreshDevicesListThread() {

        updateDevicesListsThread = () -> {


            if (!listOfAllDevices.isEmpty()) {
                pairedDevicesList.clear();
                unpairedDevicesList.clear();
                bluetoothDevicesFinder.classifyDevices(listOfAllDevices);

                ///   pairedDevicesList = bluetoothDevicesFinder.getPairedDevicesList();
                //   unpairedDevicesList = bluetoothDevicesFinder.getunpairedDevicesList();


                Log.e("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!", " ZAPOSTOWAL HANDLER " + unpairedDevicesList.size());
                foundRecyclerAdapter.notifyDataSetChanged();
                pairedRecyclerAdapter.notifyDataSetChanged();
            }

            Log.e("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!", " 222 ZAPOSTOWAL HANDLER " + pairedDevicesList.size());

        };

        blueHandler.post(updateDevicesListsThread);
    }

    void searchBtDevices() {
        Log.e("INFO_SZUKAM", "SZUKAM URZADZEN");
        bluetoothAdapter.startDiscovery();

    }// searchDevicesBT()
}//MainMenu
